# PKGBUILDs

My own PKGBUILDs for “missing” ArchLinux/AUR packages.

Until I have found out how to easily make each subdirectory a git submodule, you can build a package by cd'ing to its subdirectory and then say `makepkg`.
